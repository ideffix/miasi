package com.ideffix.miasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiasiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiasiApplication.class, args);
	}
}
